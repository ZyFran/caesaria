{
  version : 1
  map : "/maps/Miletus.sav"
  funds : 8000
  date : "-220.01.01"
  finish : "-100.01.01"
  climate : 2
	
  win :
  {
    population : 5000
    culture : 60
    prosperity : 35
    peace : 40
    favour : 40
    next : "/missions/tutorial6.briefing"
    title : "##win_procurator_title##"
    may_continue : false
    maxHouseLevel : "big_villa"
    caption : "##miletus_5_title##"
    overview :  [ "##miletus_5_overview##",
                  "@img=pictures_00017",
                  "##miletus_5_overview_2##" ]
  }
	
  empire : {
    enabled : true

    objects : {
      ship : {
        location : [ 709, 604 ]
        name : "ship"
        picture : "empire_bits_00112"
      }

      aqueduct : {
        location : [ 170, 630 ]
        name : "aqueduct"
        picture : "empire_bits_00107"
      }

      palms : {
        location : [ 490, 790 ]
        name : "palms"
        picture : "empire_bits_00111"
      }

      colosseum : {
        location : [ 735, 495 ]
        name : "colosseum"
        picture : "empire_bits_00114"
      }

      rocks : {
        location : [ 240, 440 ]
        name : "rocks"
        picture : "empire_bits_00106"
      }

      rocks2 : {
        location : [ 530, 290 ]
        name : "rocks2"
        picture : "empire_bits_00115"
      }

      rocks3 : {
        location : [ 1010, 440 ]
        name : "rocks3"
        picture : "empire_bits_00117"
      }

      palms2 : {
        location : [ 1350, 630 ]
        name : "palms2"
        picture : "empire_bits_00121"
      }
    }

    cities :
    {
      Athenae : {
        location : [ 1200, 700 ]

        land : true
        sea : true
        available : true

        sells : { wheat : 15, wine : 25, marble : 15 }
        buys : { meat: 15, oil: 15 }
      }

      Corinthus : {
        location : [ 1160, 760 ]

        sea : true
        land : true

        sells : { clay : 15, pottery : 25 }
        buys : { weapon : 15 }
      }
    }
  }
  		
  events :
  {
    population_milestone_500#messagebox : {
      date : "-350.01.01"
      population : 500
      send2scribe : true
      video : "/smk/Population1.smk"
      title : "##population_milestone##"
      text : "##500_citzens_in_city##"
    }    
    
    population_milestone_1000#messagebox : {
      date : "-350.01.01"
      population : 1000
      send2scribe : true
      video : "/smk/Population1.smk"
      title : "##population_milestone##"
      text : "##500_citzens_in_city##"
    }   
        
    population_milestone_2000#messagebox : {
      date : "-350.01.01"
      population : 2000
      send2scribe : true
      video : "/smk/Population1.smk"
      title : "##population_milestone##"
      text : "##500_citzens_in_city##"
    }   
    
    population_milestone_3000#messagebox : {
      date : "-350.01.01"
      population : 3000
      send2scribe : true
      video : "/smk/Population2.smk"
      title : "##population_milestone##"
      text : "##500_citzens_in_city##"
    }
    
    first_colosseum_work#start_work : {
      building : [ "colloseum" ]
      no_troubles : true
      type : "start_work"
      action : {
        message#messagebox : {
          send2scribe : true
          video : "/smk/1st_Glad.smk"
          title : "##working_colloseum_title##"
          text : "##working_colloseum_text##"
        }
      }
    }

    ity_indebt#city_indebt : {
      date : "-350.01.01"
      type : "city_indebt"
      count : 2
      emperorMoney : 5000
      video : "city_indebt.bik"
      text : "##city_has_runout_money##"
    }
       
    cursed_water#contaminated_water : {
      date : "-219.01.01"
      type : "contaminated_water"
      population : 500
      value : 20
      action : {
        message#messagebox : {
          send2scribe : true
          title : "##contaminated_water_title##"
          text : "##contaminated_water_text##"
        }
      }
    }

    barbarian_invasion#enemy_attack : {
      date : "-217.10.01"
      type : "enemy_attack"
      items : {
        troop_soldiers : {
          type : "etruscanSoldier"
          count : 4
          location : "random"
        }

        troop_archers : {
          type : "etruscanArcher"
          count : 2
          location : "random"
        }
      }

      text : "##enemy_attack_city##"
    }

    barbarian_attack#messagebox : {
      date : "-217.10.01"
      title : "##barbarian_attack_title##"
      text : "##barbarian_attack_text##"
      video : "/smk/spy_army.smk"
    }

    emperor_request_weapon#city_request : {
      date : "-217.09.01"
      reqtype : "good_request"
      month : 24
      good : { weapon : 10 }
      success : { favour : 10 }
      fail : { favour : -10, appendMonth : 24 }
    }

    fire_1#random_fire : {
      date : "-216.09.01"
      strong : 10
      exec : {
        "step_1.video#messagebox" : {
          title : "##city_fire_title##"
          text : "##city_fire_text##"
          video : "/smk/city_fire.smk"
        }
      }
    }

    emperor_request_furniture#city_request : {
      date : "-215.0.01"
      reqtype : "good_request"
      month : 24
      good : { furniture : 10 }
      success : { favour : 10 }
      fail : { favour : -10, appendMonth : 24 }
    }

    fire_1#random_fire : {
      date : "-215.09.01"
      strong : 8
      exec : {
        "step_1.video#messagebox" : {
          title : "##city_fire_title##"
          text : "##city_fire_text##"
          video : "/smk/city_fire.smk"
        }
      }
    }

    barbarian_invasion_2#enemy_attack : {
      date : "-214.06.01"
      type : "enemy_attack"
      items : {
        troop_soldiers : {
          type : "etruscanSoldier"
          count : 8
          location : "random"
        }

        troop_archers : {
          type : "etruscanArcher"
          count : 5
          location : "random"
        }
      }

      text : "##enemy_attack_city##"
    }

    barbarian_attack#messagebox : {
      date : "-214.06.01"
      title : "##barbarian_attack_title##"
      text : "##barbarian_attack_text##"
      video : "/smk/spy_army.smk"
    }

    collapse_1#random_collapse : {
      date : "-214.11.01"
      population : 60
      strong : 10
    }

    barbarian_invasion_3#enemy_attack : {
      date : "-211.04.01"
      type : "enemy_attack"
      items : {
        troop_soldiers : {
          type : "etruscanSoldier"
          count : 16
          location : "random"
        }

        troop_archers : {
          type : "etruscanArcher"
          count : 10
          location : "random"
        }
      }

      text : "##enemy_attack_city##"
    }

    barbarian_attack#messagebox : {
      date : "-211.04.01"
      title : "##barbarian_attack_title##"
      text : "##barbarian_attack_text##"
      video : "/smk/spy_army.smk"
    }


  }
	
  buildoptions :
  {
    farm : "disable_all"
    raw_material : "disable_all"
    factory : "disable_all"
    water : "enabled"
    health : "enabled"
    religion : "disable_all"
    education : "enabled"
    entertainment : "enabled"
    govt : "enabled"
    engineering : "disable_all"
    security : "disable_all"
    check_desirability : true
                   
    buildings : 
    { 
      engineering_post : true
      plaza : true
      clay_pit : true
      iron_mine : true
      pottery : true
      weapons_workshop : true
      oil_workshop : true
      furniture_workshop : true
      forum_1 : false
      garden : true
      low_bridge : true
      high_bridge : true
      dock : true
      wharf : true
      prefecture : true
      small_ceres_temple : true
      small_venus_temple : true
      small_mars_temple : true
      small_neptune_temple : true
      small_mercury_temple : true
      shipyard : true
      market : true
      granery : true
      warehouse : true
      school : true
      academy : true
      library : true
      senate_1 : true
      gatehouse : true
      wall : true 
      tower : true 
      fort_legionaries : true
      fort_javelin : true
      fort_horse : true
      military_academy : true
      lumber_mill = true
      barracks : true
      fruit_farm : true
      olive_farm : true
    }
  }
}
